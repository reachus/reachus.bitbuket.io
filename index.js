document.getElementById("name").onmouseover = function() { mouseOver() };
document.getElementById("name").onmouseout = function() { mouseOut() };

function mouseOver() {
    document.getElementById("name").style.color = "rgb(31, 74, 129)";
}

function mouseOut() {
    document.getElementById("name").style.color = "black";
}

document.getElementById("message").onmouseover = function() { mouseOver2() };
document.getElementById("message").onmouseout = function() { mouseOut2() };

function mouseOver2() {
    document.getElementById("message").style.color = "rgb(31, 74, 129)";
}

function mouseOut2() {
    document.getElementById("message").style.color = "black";
}